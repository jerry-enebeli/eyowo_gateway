const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
// const router = express.Router()
const AirbrakeClient = require('airbrake-js');
const fileUpload = require('express-fileupload');
require('dotenv').config();

const app = express();

const auth = require('./app/routes/v1/auth');
const users = require('./app/routes/v1/users');
const opay = require('./app/routes/v1/opay');
const business = require('./app/routes/v1/business');
const store = require('./app/routes/v1/store');
const till = require('./app/routes/v1/till');
const product = require('./app/routes/v1/product');

app.use(fileUpload());
// Enable HTTP request logging
app.use(morgan('combined'));
// Enable CORS
app.use(cors());
// CORS pre-flight
app.options('*', cors());
// Extract IP Info
app.use((req, res, next) => {
  req.remote_ip_address = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  next();
});

app.get('/', (req, res) => {
  res.send('Welcome to the Eyowo API Service!');
});
app.get('/v1', (req, res) => {
  res.send('Welcome to the Eyowo API Service!');
});
app.get('/v2', (req, res) => {
  res.send('Welcome to the Eyowo API Service!');
});

app.use('/v1/auth', auth);
app.use('/v1/users', users);
app.use('/v1/opay', opay);
app.use('/v1/business', business);
app.use('/v1/store', store);
app.use('/v1/till', till);
app.use('/v1/product', product);

const server = app.listen(process.env.SERVER_PORT || 3000, () => {
  const host = server.address().address;
  const { port } = server.address();
  process.on('uncaughtException', (err) => {
    console.error(err, 'Uncaught Exception thrown');
    process.exit(1);
  });
  console.log('Eyowo API Service listening at http://%s:%s', host, port);
});


const airbrake = new AirbrakeClient({
  projectId: 184436,
  projectKey: '4f5138863964f2bc7a03515d3d773ba6',
});
