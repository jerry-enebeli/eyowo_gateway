const grpc = require('grpc');
require('dotenv');
const PROTO_PATH = `${__dirname}/../app/routes/v1/proto/auth.proto`;

const AuthService = grpc.load(PROTO_PATH).eyowo_auth;
const AuthRPC = new AuthService.Auth('35.238.90.184:80', grpc.credentials.createInsecure());


module.exports.validateSession = function validateSession(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  if (req.params.user_id !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};

// check to ensure that the authenticated user owns the business being operated on
module.exports.validateUserOwnsBusiness = async function validateUserOwnsBusiness(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  const business = await Business.findOne({ _id: req.params.business_id });


  if (business.owner.toString() !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};

// check to ensure that the authenticated user owns the store being operated on
module.exports.validateUserOwnsStore = async function validateUserOwnsStore(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  const store = await Store.findOne({ _id: req.params.store_id });


  if (store.owner.toString() !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};

// check to ensure that the authenticated user owns the store being operated on
module.exports.validateUserOwnsAuthorizationRequest = async function validateUserOwnsAuthorizationRequest(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  const authorization = await Authorization.findOne({ _id: req.params.authorization_id });

  if (!authorization) {
    console.log('DOES NOT EXIST');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; not found',
    });
  }

  if (authorization.user.toString() !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};


module.exports.validateToken = function validateToken(req, res, next) {
  let token;
  try {
    token = req.headers.authorization;
    // console.log(req.headers['authorization']);
    // console.log(req.headers);
    // console.log(token);
    token = token.substr(7);
  } catch (err) {
    console.error(err);
    res.status(403).send({
      success: false,
      message: 'Invalid or no authorization token provided.',
    });
    return;
  }
  try {
    AuthRPC.validateAuthorizationHeader({ token }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      if (response.success) {
        response.user = JSON.parse(response.user);
        response.decoded = JSON.parse(response.decoded);
        req.token = response.decoded;
        req.eyowo_user = response.user;
        next();
        return;
      }
      res.status(response.status).send({
        success: false,
        message: response.message,
      });
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

module.exports.validateTokenTill = function validateToken(req, res, next) {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    // console.log(req.headers['authorization']);
    // console.log(req.headers);
    // console.log(token);
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: 'Invalid or no authorization token provided.',
    });
  }

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, config.tokenSecret, (err, decoded) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: 'Failed to authenticate token.',
        err,
      });
    }
    Till.findById(decoded._id, (errr, till) => {
      if (errr) {
        console.log('nothing found');
        return res.status(403).send({
          success: false,
          message: 'Authentication failed. Eyowo user not found.',
        });
      }
      // if everything is good, save to request for use in other routes
      req.token = decoded;
      req.till = till;

      // console.log(req.eyowo_user);

      next();
    });
  });
  return null;
};

module.exports.validateSetSecurePinToken = function validateSetSecurePinToken(req, res, next) {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    // console.log(req.headers['authorization']);
    // console.log(req.headers);
    // console.log(token);
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: 'Invalid or no authorization token provided.',
    });
  }

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, config.tokenSecret, (err, decoded) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: 'Failed to authenticate token.',
        err,
      });
    }
    if (decoded.usage === undefined) {
      return res.status(403).send({
        success: false,
        message: 'Invalid token rights.',
      });
    }
    if (decoded.usage !== 'setpin') {
      return res.status(403).send({
        success: false,
        message: 'Token cannot perform this action.',
      });
    }
    /*
    User.findById(decoded._id, (errr, user) => {
      if (errr) {
        console.log('nothing found');
        return res.status(403).send({
          success: false,
          message: 'Authentication failed. Eyowo user not found.',
        });
      }
      // if everything is good, save to request for use in other routes
      req.token = decoded;
      req.eyowo_user = user;

      // console.log(req.eyowo_user);

      next();
    }); */
    req.set_securepin_token = decoded;

    // console.log(req.eyowo_user);

    next();
  });
  return null;
};
