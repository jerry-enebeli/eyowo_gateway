const express = require('express');
const protoLoader = require('@grpc/proto-loader');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');

const PROTO_PATH = `${__dirname}/proto/auth.proto`;

const USER_PROTO_PATH = `${__dirname}/proto/user.proto`;

const UserService = grpc.load(USER_PROTO_PATH).eyowo_user;
const UserRPC = new UserService.User('35.232.39.202:80', grpc.credentials.createInsecure());


const packageDefinition = protoLoader.loadSync(
  PROTO_PATH,
  {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
  },
);

// const AuthService = grpc.load(PROTO_PATH).eyowo_auth;
const AuthService = grpc.loadPackageDefinition(packageDefinition).eyowo_auth;

// const AuthService = grpc.load(PROTO_PATH).eyowo_auth;
const AuthRPC = new AuthService.Auth(`${process.env.AUTH_SERVICE_IP}:${process.env.AUTH_SERVICE_PORT}`, grpc.credentials.createInsecure());

const router = express.Router();


// TEST ROUTE
router.get('/', (req, res) => {
  AuthRPC.testMessage({ mobile: '2348066953623' }, (err, response) => {
    if (err) {
      console.log(err);
    }
    console.log(response);
    res.json(response);
  });
});


router.post('/validate/mobile', bodyParser.json(), (req, res) => {
  console.log(req.body);

  // Validate request
  const schemaValidate = Joi.object().keys({
    mobile: Joi.string().regex(/^[0-9]{9,13}$/).required(),
  });

  const validateResult = Joi.validate(req.body, schemaValidate);

  if (validateResult.error !== null) {
    return res.json({
      success: false,
      message: validateResult.error.details[0].message,
    });
  }

  try {
    AuthRPC.validateMobile({ mobile: req.body.mobile }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
});

router.post('/validate/securepin', bodyParser.json(), async (req, res) => {
  const schemaValidate = Joi.object().keys({
    mobile: Joi.string().regex(/^[0-9]{9,13}$/).required(),
    secure_pin: Joi.string().regex(/^[0-9]{6}$/).required(),
  });

  const validateResult = Joi.validate(req.body, schemaValidate);

  if (validateResult.error !== null) {
    return res.json({
      success: false,
      message: validateResult.error.details[0].message,
    });
  }

  try {
    AuthRPC.validateSecurePIN({ mobile: req.body.mobile, secure_pin: req.body.secure_pin }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      if (response.success) {
        response.user = JSON.parse(response.user);
      }
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
});

router.post('/reauthorize/token', bodyParser.json(), (req, res) => {
  // console.log(req.body);

  // Validate request
  const schemaValidate = Joi.object().keys({
    mobile: Joi.string().regex(/^[0-9]{9,13}$/).required(),
    previous_token: Joi.string().required(),
  });

  const validateResult = Joi.validate(req.body, schemaValidate);

  if (validateResult.error !== null) {
    return res.json({
      success: false,
      message: validateResult.error.details[0].message,
    });
  }

  try {
    AuthRPC.reAuthorizeToken({ mobile: req.body.mobile, previous_token: req.body.previous_token }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      if (response.success) {
        response.user = JSON.parse(response.user);
      }
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
});

 router.put('/forgetpassword', bodyParser.json(), (req, res) =>{
  try {
    UserRPC.recoverPassword({ usermobile: req.body.user_mobile, mobile: req.body.recovery_mobile }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
});


router.post('/validate/sms', bodyParser.json(), (req, res) => {
  try {
    AuthRPC.validateSms({ mobile: req.body.mobile }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
});

module.exports = router;
