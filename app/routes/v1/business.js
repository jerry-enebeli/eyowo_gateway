const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');
const middlewareAuth = require('../../../middleware/auth');
const { singleUpload } = require('../../config/config');


const PROTO_PATH = `${__dirname}/proto/business.proto`;

const businessService = grpc.load(PROTO_PATH).Business;
const businessRPC = new businessService.Business('35.239.191.61:80', grpc.credentials.createInsecure());

const router = express.Router();

const createBusiness = async (req, res) => {
  try {
    const file = await singleUpload(req.files);
    req.body.image = file.url;
    businessRPC.createBusiness({ body: JSON.stringify(req.body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
    
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const getBusiness = async (req, res) => {
  console.log(req.params);
  const phone = JSON.stringify(req.params);
  const ownerId = JSON.stringify(req.params.ownerId);

  try {
    businessRPC.getBusiness({ phone, ownerId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const getProduct = async (req, res) => {
  const ussd = JSON.stringify(req.query.ussd);
  try {
    businessRPC.getProduct({ ussd }, (err, response) => {
      if (err) {
        throw err;
      }
      response.data = JSON.parse(response.data);
      res.status(200).json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const Activity = async (req, res) => {

  try {
    const ownerId = JSON.stringify(req.params.ownerId);
    console.log(ownerId);
    businessRPC.Activity({ ownerId }, (err, response) => {
      if (err) {
        throw err;
      }
      response.data = JSON.parse(response.data);
      res.status(200).json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const getAll = async (req, res) => {
  try {
    const ownerId = JSON.stringify(req.params.ownerId);
    businessRPC.getAll({ ownerId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const updateBusiness = async (req, res) => {
  console.log(req.body);
  const file = await singleUpload(req.files);
  req.body.image = file.url;
  const phone = JSON.stringify(req.params);
  const ownerId = JSON.stringify(req.params.ownerId);

  const body = req.body;

  try {
    businessRPC.updateBusiness({ phone, ownerId, body: JSON.stringify(body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      // response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const deleteBusiness = async (req, res) => {
  console.log(req.params);

  const phone = JSON.stringify(req.params);
  const ownerId = JSON.stringify(req.params.ownerId);

  try {
    businessRPC.deleteBusiness({ phone, ownerId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};
// ************************************************************************************************************************
//
//                  Authenticate all routes from here ON!!!!!
//
// ************************************************************************************************************************
// router.use(middlewareAuth.validateToken);

router.post('/', [bodyParser.json(), createBusiness]);

router.get('/owner/:ownerId/:phone', getBusiness);

router.get('/activity/:ownerId', Activity);

router.get('/ussd', getProduct);

router.get('/owner/:ownerId', getAll);

router.put('/:ownerId/:phone', [bodyParser.json(), updateBusiness]);

router.delete('/:ownerId/:phone', deleteBusiness);

module.exports = router;
