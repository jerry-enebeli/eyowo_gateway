const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');
const middlewareAuth = require('../../../middleware/auth');
const { singleUpload } = require('../../config/config');


const PROTO_PATH = `${__dirname}/proto/store.proto`;

const storeService = grpc.load(PROTO_PATH).Store;
const businessRPC = new storeService.Store('35.239.191.61:80', grpc.credentials.createInsecure());

const router = express.Router();


const createStore = async (req, res) => {
  try {
    const file = await singleUpload(req.files);
    req.body.image = file.url;

    businessRPC.createStore({ body: JSON.stringify(req.body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const getStore = async (req, res) => {
  const phone = JSON.stringify(req.params.phone);
  const businessId = JSON.stringify(req.params.businessId);
  console.log(phone)
  try {
    businessRPC.getStore({ phone, businessId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const getAll = async (req, res) => {
  try {
    const businessId = JSON.stringify(req.params.businessId);
    businessRPC.getAll({ businessId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const updateStore = async (req, res) => {
  console.log(req.body);

  const phone = JSON.stringify(req.params.phone);
  const businessId = JSON.stringify(req.params.businessId);
  const file = await singleUpload(req.files);
  req.body.image = file.url;

  const body = req.body;

  try {
    businessRPC.updateStore({ phone, businessId, body: JSON.stringify(body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const deleteStore = async (req, res) => {
  console.log(req.params)
  const phone = JSON.stringify(req.params.phone);
  const businessId = JSON.stringify(req.params.businessId);
  console.log(businessId)
  try {
    businessRPC.deleteStore({ phone, businessId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const nearBy = async (req, res) => {
  try {
    console.log(req.body);
    businessRPC.nearBy({ body: JSON.stringify(req.body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
       response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

// ************************************************************************************************************************
//
//                  Authenticate all routes from here ON!!!!!
//
// ************************************************************************************************************************
// router.use(middlewareAuth.validateToken);

router.post('/', [bodyParser.json(), createStore]);

router.get('/:businessId/:phone', getStore);

router.get('/:businessId', getAll);

router.put('/:businessId/:phone', [bodyParser.json(), updateStore]);

router.delete('/:businessId/:phone', deleteStore);

router.post('/nearBy', [bodyParser.json(), nearBy]);

module.exports = router;
