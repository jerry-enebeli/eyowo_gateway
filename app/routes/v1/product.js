const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');
const middlewareAuth = require('../../../middleware/auth');
const { singleUpload } = require('../../config/config');


const PROTO_PATH = `${__dirname}/proto/product.proto`;

const productService = grpc.load(PROTO_PATH).Product;
const businessRPC = new productService.Product('35.239.191.61:80', grpc.credentials.createInsecure());

const router = express.Router();


const createProduct = async (req, res) => {
  console.log(req.body);
  const file = await singleUpload(req.files);
  req.body.image = file.url;
  try {
    businessRPC.createProduct({ body: JSON.stringify(req.body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const getproduct = async (req, res) => {
  const productId = JSON.stringify(req.params.productId);
  const businessId = JSON.stringify(req.params.businessId);

  try {
    businessRPC.getproduct({ productId, businessId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};


const getproducts = async (req, res) => {
  try {
    const businessId = JSON.stringify(req.params.businessId);
    console.log(businessId);

    businessRPC.getproducts({ businessId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const updateproduct = async (req, res) => {
  console.log(req.body);

  const productId = JSON.stringify(req.params.productId);
  const businessId = JSON.stringify(req.params.businessId);
  const file = await singleUpload(req.files);
  req.body.image = file.url;

  const { body } = req;

  try {
    businessRPC.updateproduct({ productId, businessId, body: JSON.stringify(body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const deleteproduct = async (req, res) => {
  console.log(req.params);

  const productId = JSON.stringify(req.params.productId);
  const businessId = JSON.stringify(req.params.businessId);

  try {
    businessRPC.deleteproduct({ productId, businessId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};
// ************************************************************************************************************************
//
//                  Authenticate all routes from here ON!!!!!
//
// ************************************************************************************************************************
// router.use(middlewareAuth.validateToken);

router.post('/', [bodyParser.json(), createProduct]);


router.get('/:businessId/:productId', getproduct);

router.get('/:businessId', getproducts);

router.put('/:businessId/:productId', [bodyParser.json(), updateproduct]);

router.delete('/:businessId/:productId', deleteproduct);

module.exports = router;
