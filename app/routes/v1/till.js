const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const middlewareAuth = require('../../../middleware/auth');


const PROTO_PATH = `${__dirname}/proto/till.proto`;

const tillService = grpc.load(PROTO_PATH).Till;
const businessRPC = new tillService.Till('35.239.191.61:80', grpc.credentials.createInsecure());

const router = express.Router();


const createTill = async (req, res) => {
  console.log(req.body);

  try {
    businessRPC.createTill({ body: JSON.stringify(req.body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const getTill = async (req, res) => {
  console.log(req.params.storeid);
  const phone = JSON.stringify(req.params.phone);
  const businessId = JSON.stringify(req.params.businessId);
  const storeId = JSON.stringify(req.params.storeId);

  try {
    businessRPC.getTill({ phone, businessId, storeId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const tillLogin = async (req, res) => {
  console.log(req.body);
  try {
    businessRPC.tillLogin({ body: JSON.stringify(req.body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      // response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const gettills = async (req, res) => {
  try {
    const businessId = JSON.stringify(req.params.businessId);
    const storeId = JSON.stringify(req.params.storeId);
    console.log(businessId);

    businessRPC.gettills({ businessId, storeId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const updateTill = async (req, res) => {
  console.log(req.body);

  const phone = JSON.stringify(req.params.phone);
  const businessId = JSON.stringify(req.params.businessId);
  const storeId = JSON.stringify(req.params.storeId);
  console.log(phone);

  const body = req.body;

  try {
    businessRPC.updateTill({
 phone, businessId, storeId, body: JSON.stringify(body)
 }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

const deleteTill = async (req, res) => {
  console.log(req.params);

  const phone = JSON.stringify(req.params.phone);
  const businessId = JSON.stringify(req.params.businessId);
  const storeId = JSON.stringify(req.params.storeId);

  try {
    businessRPC.deleteTill({ phone, businessId, storeId }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

// ************************************************************************************************************************
//
//                  Authenticate all routes from here ON!!!!!
//
// ************************************************************************************************************************
// router.use(middlewareAuth.validateToken);

router.post('/', [bodyParser.json(), createTill]);

router.post('/login', [bodyParser.json(), tillLogin]);

router.get('/:businessId/:storeId/:phone', getTill);

router.get('/:businessId/:storeId', gettills);

router.put('/:businessId/:storeId/:phone', [bodyParser.json(), updateTill]);

router.delete('/:businessId/:storeId/:phone', deleteTill);

module.exports = router;
