const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');
const middlewareAuth = require('../../../middleware/auth');

const PROTO_PATH = `${__dirname}/proto/opay.proto`;

const OpayService = grpc.load(PROTO_PATH).payment;
const OpayRPC = new OpayService.Opay(
  '35.193.246.114:80',
  grpc.credentials.createInsecure(),
);

const router = express.Router();

async function InitPayment(req, res) {
  try {
    const schema = Joi.object().keys({
      cardNumber: Joi.string().required(),
      cardDateMonth: Joi.string().required(),
      cardDateYear: Joi.string().required(),
      cardCVC: Joi.string().required(),
    });

    const validateResult = Joi.validate(req.body, schema);
    console.log('here')
    if (validateResult.error !== null) {
      res.json({
        success: false,
        message: validateResult.error.details[0].message,
      });
      return;
    }
    OpayRPC.InitPayment(
      {
        cardNumber: req.body.cardNumber,
        cardDateMonth: req.body.cardDateMonth,
        cardDateYear: req.body.cardDateYear,
        cardCVC: req.body.cardCVC,
        amount: 100,
        mobile: '2348141804018',
      },
      (err, resp) => {
        if (err) {
          throw err;
        } else {
          resp.data = JSON.parse(resp.data);
          res.json(resp);
        }
      },
    );
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

async function InputPin(req, res) {
  try {
    const schema = Joi.object().keys({
      token: Joi.string().required(),
      pin: Joi.string().required(),
    });

    const validateResult = Joi.validate(req.body, schema);

    if (validateResult.error !== null) {
      res.json({
        success: false,
        message: validateResult.error.details[0].message,
      });
      return;
    }
    OpayRPC.InputPin({ token: req.body.token, pin: req.body.pin }, (err, resp) => {
      if (err) throw err;

      res.json(resp);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

async function InputOtp(req, res) {
  try {
    const schema = Joi.object().keys({
      token: Joi.string().required(),
      otp: Joi.string().required(),
    });

    const validateResult = Joi.validate(req.body, schema);

    if (validateResult.error !== null) {
      res.json({
        success: false,
        message: validateResult.error.details[0].message,
      });
      return;
    }
    OpayRPC.InputOtp({ token: req.body.token, otp: req.body.otp }, (err, resp) => {
      if (err) throw err;

      res.json(resp);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

async function chargIntrumentId(req, res) {
  try {
    const schema = Joi.object().keys({
      instrument_id: Joi.string().required(),
      amount: Joi.number().integer().required(),
    });

    const validateResult = Joi.validate(req.body, schema);

    if (validateResult.error !== null) {
      res.json({
        success: false,
        message: validateResult.error.details[0].message,
      });
      return;
    }
    OpayRPC.ChargeIntrumentId({ id: req.body.instrument_id, amount: req.body.amount }, (err, resp) => {
      if (err) throw err;

      res.json(resp);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

// ************************************************************************************************************************
//
//                  Authenticate all routes from here ON!!!!!
//
// ************************************************************************************************************************
router.use(middlewareAuth.validateToken);

router.post('/:user_id/initiatecard', [bodyParser.json()], InitPayment);

router.post('/:user_id/inputpin', [bodyParser.json()], InputPin);

router.post('/:user_id/inputotp', [bodyParser.json()], InputOtp);

router.post('/:user_id/chargeid', [bodyParser.json()], chargIntrumentId);

module.exports = router;
