const express = require('express');
const protoLoader = require('@grpc/proto-loader');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');


let PROTO_PATH = `${__dirname}/proto/providus.proto`;

let packageDefinition = protoLoader.loadSync(
  PROTO_PATH,
  {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
  },
);

const ProvidusService = grpc.loadPackageDefinition(packageDefinition).payment;

// const ProvidusService = grpc.load(providusPROTOPATH).payment;
const ProvidusRPC = new ProvidusService.Providus(`${process.env.PROVIDUS_SERVICE_IP}:${process.env.PROVIDUS_SERVICE_PORT}`, grpc.credentials.createInsecure());


PROTO_PATH = `${__dirname}/proto/etranzact.proto`;

packageDefinition = protoLoader.loadSync(
  PROTO_PATH,
  {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
  },
);

const eTranzactService = grpc.loadPackageDefinition(packageDefinition).eTranzact;

// const ProvidusService = grpc.load(providusPROTOPATH).payment;
const eTranzactRPC = new eTranzactService.eTranzact(`${process.env.ETRANZACT_SERVICE_IP}:${process.env.ETRANZACT_SERVICE_PORT}`, grpc.credentials.createInsecure());

const router = express.Router();

async function validateBVN(req, res) {
  try {
    ProvidusRPC.GetBVNDetails({ bvn: req.query.bvn }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      /*
      if (response.success) {
        response.beneficiaries = JSON.parse(response.beneficiaries);
      } */
      console.log(JSON.parse(response.data));
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
}

async function getBanks(req, res) {
  if (req.query.from === 'providus') {
    try {
      ProvidusRPC.GetNIPBanks({}, (err, response) => {
        if (err) {
          throw err;
        }
        console.log(response);
        /*
      if (response.success) {
        response.beneficiaries = JSON.parse(response.beneficiaries);
      } */
        response.data = JSON.parse(response.data);
        res.json(response);
      });
    } catch (err) {
      console.error(err);
      res.status(500).send({ success: false, err });
    }
  } else if (req.query.from === 'etranzact') {
    try {
      eTranzactRPC.getBanks({}, (err, response) => {
        if (err) {
          throw err;
        }
        console.log(response);
        /*
      if (response.success) {
        response.beneficiaries = JSON.parse(response.beneficiaries);
      } */
        // response.data = JSON.parse(response.data);
        res.json(response);
      });
    } catch (err) {
      console.error(err);
      res.status(500).send({ success: false, err });
    }
  } else {
    res.status(400).send({ success: false, err: 'Need to specify source as from. e.g ?from=providus' });
  }
}

async function getBankAccountInfo(req, res) {
  if (req.query.from === 'providus') {
    try {
      ProvidusRPC.GetNIPAccount({ account: req.query.account, bank_code: req.query.bank_code }, (err, response) => {
        if (err) {
          throw err;
        }
        console.log(response);
        /*
      if (response.success) {
        response.beneficiaries = JSON.parse(response.beneficiaries);
      } */
        response.data = JSON.parse(response.data);
        res.json(response);
      });
    } catch (err) {
      console.error(err);
      res.status(500).send({ success: false, err });
    }
  } else {
    res.status(400).send({ success: false, err: 'Need to specify source as from. e.g ?from=providus' });
  }
}


router.get('/bvn', validateBVN);
router.get('/banks', getBanks);
router.get('/bankaccount', getBankAccountInfo);

module.exports = router;
