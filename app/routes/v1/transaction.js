const express = require('express');
const grpc = require('grpc');
const bodyParser = require('body-parser');
const Joi = require('joi');
const middlewareAuth = require('../../../middleware/auth');


const PROTO_PATH = `${__dirname}/proto/till.proto`;

const tillService = grpc.load(PROTO_PATH).Till;
const businessRPC = new tillService.Till('35.239.191.61:80', grpc.credentials.createInsecure());

const router = express.Router();

const addTillTransaction = async (req, res) => {
  try {
    businessRPC.addTillTransaction({ body: JSON.stringify(req.body) }, (err, response) => {
      if (err) {
        throw err;
      }
      console.log(response);
      response.data = JSON.parse(response.data);
      res.json(response);
    });
  } catch (err) {
    console.error(err);
    res.status(500).send({ success: false, err });
  }
};

// ************************************************************************************************************************
//
//                  Authenticate all routes from here ON!!!!!
//
// ************************************************************************************************************************
// router.use(middlewareAuth.validateToken);

router.post('/', [bodyParser.json(), addTillTransaction]);

module.exports = router;
