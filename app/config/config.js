const cloudinary = require('cloudinary');
const mongoose = require('mongoose');

cloudinary.config({
  cloud_name: 'xandra',
  api_key: '638981457739382',
  api_secret: 'INTeFtnSsnuef63ySMISpbT9XAw',
});

exports.singleUpload = function singleUpload(file) {
  if (!file) {
    return 'no file uploaded';
  }
  // console.log(file);

  const type = file.image.mimetype.split('/');


  switch (type[0]) {
    // eslint-disable-next-line
    case 'image':
      const entryimage = file.image;
      const imageId = new mongoose.mongo.ObjectID();
      const extension = entryimage.name.split('.').pop();
      return new Promise((resolve, reject) => {
        entryimage.mv(`/tmp/${imageId}.${extension}`, () => {
          cloudinary.uploader.upload(`/tmp/${imageId}.${extension}`, (result) => {
            console.log(result);
            resolve(result);
          }, {
            public_id: imageId,
          });
        });
      });

    // eslint-disable-next-line
    case 'video':
      const entryvideo = file.image;
      const videoId = new mongoose.mongo.ObjectID();
      const extensions = entryvideo.name.split('.').pop();
      return new Promise((resolve, reject) => {
        entryvideo.mv(`/tmp/${videoId}.${extensions}`, () => {
          cloudinary.uploader.upload(`/tmp/${videoId}.${extensions}`, (result) => {
            console.log(result);
            resolve(result);
          }, {
            resource_type: 'video',
            use_filename: true,
            unique_filename: true,
            backup: true,
            tags: ['logo', 'for_logo'],
          }, {
            public_id: videoId,
          });
        });
      });

    // eslint-disable-next-line
    case 'application':
      const entrydocument = file.image;
      const documentId = new mongoose.mongo.ObjectID();
      const extend = entrydocument.name.split('.').pop();
      return new Promise((resolve, reject) => {
        entrydocument.mv(`/tmp/${documentId}.${extend}`, () => {
          cloudinary.uploader.upload(`/tmp/${documentId}.${extend}`, (result) => {
            console.log(result);
            resolve(result);
          }, {
            resource_type: 'auto',
            use_filename: true,
            unique_filename: true,
            backup: true,
            tags: ['logo', 'for_logo'],
          }, {
            public_id: documentId,
          });
        });
      });

      // eslint-disable-next-line
    default:
      console.log('Cloudinary Error');
      break;
  }
};
